# Web Applications Architecture #

Este repositório foi criado para acompanhar o curso Web Applications Architecture oferecido pela [Coursera](https://www.coursera.org/)

### Tecnologias envolvidas ###

* [Ruby On Rails](http://rubyonrails.org/) - Framework de desenvolvimento web
* [Nitrous.io](https://www.nitrous.io/) - Ambiente na nuvem para desenvolvimento

### Resumo ###

* Sensacional o curso, aplica as teorias de uma forma prática, facilitando o aprendizado.
* Instrutor muito bom.
* Tecnologias atualizadas para aprendizado (desenvolvimento com windows nunca mais vai ser o mesmo depois do Nitrous).
* Acrescentou muito a teoria das arquiteturas de um aplicativo web, além dos conceitos do Rails em si.

### Indicação para quem for fazer o curso ###

O curso é excelente, muito bom mesmo, mas ele parte do principio que já se tenha alguma base de conhecimento, ele não entra em alguns critérios a fundo, aconselho então ter uma base nas seguintes tecnologias:

* Ruby e Ruby On Rails - No curso entra com alguns detalhes sobre a linguagem e sobre o framework, porém, acho que aproveitei mais por já conhecer e ter desenvolvido nessas tecnologias.
* Bancos de dados - Tem que ter o conhecimento acerca de bases de dados relacionais, o curso não vai detalhar e nem explicar o que são bancos de dados.
* SQL - O curso não entra em detalhes sobre SQL, mas se tu já entende, é legal ver como o Active Record utiliza esse recurso na sua base.

Além das tecnologias, gostaria de dar as seguintes sugestões:

* Fazer todos os deveres de casas sem trapacear - Eles ajudam a firmar aquilo que foi aprendido na aula e "colar" nos deveres de casa não vai te ajudar, ter uma nota melhor não vai te fazer um profissional melhor, leve em consideração isso.
* Participar das discussões - Como o curso já foi encerrado, não temos muito o que fazer se não ver o que já foi postado, caso tenha dúvidas ou algo do gênero, destaquei essa situação, pois o ideal é sempre participar, seja perguntando ou respondendo ou simplesmente compartilhando alguma ideia, nos próximos cursos quero tentar seguir o ritmo da aula conforme os cronogramas já definidos.